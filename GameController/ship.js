class Ship {
    constructor(name, size, color) {
        this.name = name;
        this.size = size;
        this.color = color;
        this.positions = [];
    }

    addPosition(position, ships = []) {
        this.positions.push(position);
    }

    deletePosition(position) {
        var index = this.positions.indexOf(position);
        this.positions.splice(index, 1);
        this.size--;
    }
}

module.exports = Ship;
