class Position {
    constructor(column, row) {
        this.column = column;
        this.row = row;
    }

    toString() {
        if(this.column && this.row) return this.column.toString() + this.row.toString()
        else return ''
    }

}

module.exports = Position;