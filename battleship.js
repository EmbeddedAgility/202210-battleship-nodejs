const { Worker, isMainThread } = require('worker_threads');
const readline = require('readline-sync');
const gameController = require("./GameController/gameController.js");
const cliColor = require('cli-color');
const beep = require('beepbeep');
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");
let telemetryWorker;
var maxRows = 8;
var maxColumns = 8;
var whiteListedPositionsForAttacking = [];

class Battleship {
    start() {
        telemetryWorker = new Worker("./TelemetryClient/telemetryClient.js");

        console.log("Starting...");
        telemetryWorker.postMessage({ eventName: 'ApplicationStarted', properties: { Technology: 'Node.js' } });

        console.log(cliColor.magenta("                                     |__"));
        console.log(cliColor.magenta("                                     |\\/"));
        console.log(cliColor.magenta("                                     ---"));
        console.log(cliColor.magenta("                                     / | ["));
        console.log(cliColor.magenta("                              !      | |||"));
        console.log(cliColor.magenta("                            _/|     _/|-++'"));
        console.log(cliColor.magenta("                        +  +--|    |--|--|_ |-"));
        console.log(cliColor.magenta("                     { /|__|  |/\\__|  |--- |||__/"));
        console.log(cliColor.magenta("                    +---------------___[}-_===_.'____                 /\\"));
        console.log(cliColor.magenta("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _"));
        console.log(cliColor.magenta(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7"));
        console.log(cliColor.magenta("|                        Welcome to Battleship                         BB-61/"));
        console.log(cliColor.magenta(" \\_________________________________________________________________________|"));
        console.log();

        this.InitializeGame();
        this.StartGame();
    }

    StartGame() {
        console.clear();
        console.log(cliColor.magenta("                  __"));
        console.log(cliColor.magenta("                 /  \\"));
        console.log(cliColor.blue("           .-.  |    |"));
        console.log(cliColor.blue("   *    _.-'  \\  \\__/"));
        console.log(cliColor.green("    \\.-'       \\"));
        console.log(cliColor.green("   /          _/"));
        console.log(cliColor.red("  |      _  /"));
        console.log(cliColor.red("  |     /_\\'"));
        console.log(cliColor.yellow("   \\    \\_/"));
        console.log(cliColor.yellow("    \"\"\"\""));

        console.log(cliColor.white("================================================="));

        do {
            console.log();
            console.log(cliColor.greenBright("Player, it's your turn"));
            this.showCurEnemyState(this.enemyFleet);
            console.log(cliColor.greenBright("Enter coordinates for your shot (i.e A3) :"));
            var position = Battleship.ParsePosition(readline.question());
            while (gameController.isOutOfRange(position)) {
                console.log();
                console.log("Position is outside the playing field.");
                console.log("Try again:");
                var position = Battleship.ParsePosition(readline.question());
            };

            var isHit = gameController.CheckIsHit(this.enemyFleet, position);

            telemetryWorker.postMessage({ eventName: 'Player_ShootPosition', properties: { Position: position.toString(), IsHit: isHit } });

            this.shipShotDisplay(isHit, 0);
            console.log();
            console.log(isHit ? cliColor.greenBright("Yeah ! Nice hit !") : cliColor.redBright("Miss"));
            console.log()
            console.log(cliColor.white("- - - - - - - - - - - - - - - - - - - - - - - - -"));

            if (this.checkGameEnd(this.enemyFleet)) {
                // console.log("You are the winner!");
                this.endGameCoolMessage(true);
                process.exit();
            }

            console.log();
            console.log(cliColor.yellowBright("Computer, it's your turn"));

            var computerPos = this.GetRandomPosition();
            var isHit = gameController.CheckIsHit(this.myFleet, computerPos);

            telemetryWorker.postMessage({ eventName: 'Computer_ShootPosition', properties: { Position: computerPos.toString(), IsHit: isHit } });

            console.log();

            this.shipShotDisplay(isHit, 1);
            console.log();
            console.log(cliColor.yellowBright(`Computer shot in ${String.fromCharCode(64 + parseInt(computerPos.column))}${computerPos.row} and ` + (isHit ? cliColor.bgRedBright(`has hit your ship !`) : cliColor.bgGreenBright(`miss`))));
            console.log()
            console.log(cliColor.white("================================================="));

            if (this.checkGameEnd(this.myFleet)) {
                // console.log("You lose!");
                endGameCoolMessage(false);
                process.exit();
            }

        }
        while (true);
    }

    shipShotDisplay(isHit, playerColor) {
        if (isHit) {
            beep();
            console.log(cliColor.redBright("                \\         .  ./"));
            console.log(cliColor.redBright("              \\      .:\";'.:..\"   /"));
            console.log(cliColor.redBright("                  (M^^.^~~:.'\")."));
            console.log(cliColor.redBright("            -   (/  .    . . \\ \\)  -"));
            console.log(cliColor.redBright("               ((| :. ~ ^  :. .|))"));
            console.log(cliColor.redBright("            -   (\\- |  \\ /  |  /)  -"));
            console.log(cliColor.redBright("                 -\\  \\     /  /-"));
            console.log(cliColor.redBright("                   \\  \\   /  /"));
        } else {
            console.log(cliColor.blueBright("                \\         .  ./"));
            console.log(cliColor.blueBright("              \\      .:\";'.:..\"   /"));
            console.log(cliColor.blueBright("                  (M^^.^~~:.'\")."));
            console.log(cliColor.blueBright("            -   (/  .    . . \\ \\)  -"));
            console.log(cliColor.blueBright("               ((| :. ~ ^  :. .|))"));
            console.log(cliColor.blueBright("            -   (\\- |  \\ /  |  /)  -"));
            console.log(cliColor.blueBright("                 -\\  \\     /  /-"));
            console.log(cliColor.blueBright("                   \\  \\   /  /"));
        }

    }

    static endGameCoolMessage(playerWon) {
        var player = require('play-sound');
        var goodMessages = ['Flawless victory, you won! But at what cost? :/',
            'You are really good, but can you do even better?',
            'Great job, you look like a future battleship master! Keep working on your skill <3'];
        var badMessages = ['Unfortunately, you lost all your ships. Try playing again, to confirm, you are not better than the computer. :/',
            'I wouldn\'t tell my friends about this',
            'You lost. Can you at least try to do better than this?'];

        var messageIndex = Math.floor(Math.random() * goodMessages.length);
        switch (playerWon) {
            case true:
                // player.play('flawless-victory.mp3', function(err){
                //     if(err) throw err;
                // });
                // console.log(cliColor.redBright(""));
                console.log(cliColor.greenBright.bold(cliColor.bgBlack(goodMessages[messageIndex])));
                break;
            case false:
                // player.play('defeat.mp3', function(err){
                //     if(err) throw err;
                // });
                console.log(cliColor.redBright.bold(cliColor.bgBlack(badMessages[messageIndex])));
                break;
        }



    }



    checkGameEnd(ships) {
        return !ships.some(item => item.size > 0);
    }

    showCurEnemyState(ships) {
        let sunk = "Sunk: ";
        let left = "Left: ";

        for (let i = 0; i < ships.length; i++) {
            if (ships[i].size === 0) {
                sunk += ships[i].name + ", ";
            } else {
                left += ships[i].name + ", ";
            }
        }

        console.log(sunk);
        console.log(left);
    }

    static ParsePosition(input) {
        if(input === "DracarysBurnThemAll")
        {
            Battleship.dracarys();
            console.log("");
            Battleship.endGameCoolMessage(true);
            process.exit();
        }

        var letter = letters.get(input.toUpperCase().substring(0, 1));
        var number = parseInt(input.substring(1, 2), 10);
        return new position(letter, number);
    }

    GetRandomPosition() {
        var rndPositionIndex = Math.floor((Math.random() * whiteListedPositionsForAttacking.length));
        var positionString = whiteListedPositionsForAttacking.at(rndPositionIndex);
        whiteListedPositionsForAttacking.splice(rndPositionIndex, 1);

        var letter = positionString.charAt(0);
        var number = parseInt(positionString.substring(1));
        var result = new position(letter, number);
        return result;
    }

    InitializeGame() {
        this.fillWhitelistList();
        this.InitializeMyFleet();
        this.InitializeEnemyFleet();
    }


    InitializeMyFleet() {
        function isGoodPosition(position, ships = []) {
            //call Denis and Sebastians function


            //check ships overlapping
            for (const ship of ships) {
                for (const pos of ship.positions) {
                    if (pos.toString() === position.toString()) {
                        console.log("Position overlapping, try again!");
                        return false;
                    }
                }
            }

            return true;
        }

        this.myFleet = gameController.InitializeShips();

        console.log(cliColor.greenBright("Please position your fleet (Game board size is from A to H and 1 to 8) :"));

        this.myFleet.forEach((ship) => {
            console.log();
            console.log(`Please enter the positions for the ${ship.name} (size: ${ship.size})`);
            let i = 0;
            while (i < ship.size) {
                console.log(`Enter position ${i} of ${ship.size} (i.e A3):`);
                const position = Battleship.ParsePosition(readline.question());
                //console.log(this.myFleet)
                if (isGoodPosition(position, this.myFleet)) {
                    i++;
                    telemetryWorker.postMessage({ eventName: 'Player_PlaceShipPosition', properties: { Position: position, Ship: ship.name, PositionInShip: i } });
                    ship.addPosition(position);
                }
            }
        })
    }

    InitializeEnemyFleet() {
        this.enemyFleet = gameController.InitializeShips();

        let rand = Math.floor(Math.random() * 3) + 1;

        switch(rand) {
            case 1: 
                this.enemyFleet[0].addPosition(new position(letters.H, 4));
                this.enemyFleet[0].addPosition(new position(letters.H, 5));
                this.enemyFleet[0].addPosition(new position(letters.H, 6));
                this.enemyFleet[0].addPosition(new position(letters.H, 7));
                this.enemyFleet[0].addPosition(new position(letters.H, 8));
        
                this.enemyFleet[1].addPosition(new position(letters.C, 6));
                this.enemyFleet[1].addPosition(new position(letters.C, 7));
                this.enemyFleet[1].addPosition(new position(letters.C, 8));
                this.enemyFleet[1].addPosition(new position(letters.C, 9));
        
                this.enemyFleet[2].addPosition(new position(letters.A, 3));
                this.enemyFleet[2].addPosition(new position(letters.B, 3));
                this.enemyFleet[2].addPosition(new position(letters.C, 3));
        
                this.enemyFleet[3].addPosition(new position(letters.D, 8));
                this.enemyFleet[3].addPosition(new position(letters.E, 8));
                this.enemyFleet[3].addPosition(new position(letters.F, 8));
        
                this.enemyFleet[4].addPosition(new position(letters.F, 1));
                this.enemyFleet[4].addPosition(new position(letters.F, 2));
                break;

            case 2:
                this.enemyFleet[0].addPosition(new position(letters.D, 4));
                this.enemyFleet[0].addPosition(new position(letters.D, 5));
                this.enemyFleet[0].addPosition(new position(letters.D, 6));
                this.enemyFleet[0].addPosition(new position(letters.D, 7));
                this.enemyFleet[0].addPosition(new position(letters.D, 8));
        
                this.enemyFleet[1].addPosition(new position(letters.A, 6));
                this.enemyFleet[1].addPosition(new position(letters.A, 7));
                this.enemyFleet[1].addPosition(new position(letters.A, 8));
                this.enemyFleet[1].addPosition(new position(letters.A, 9));
        
                this.enemyFleet[2].addPosition(new position(letters.F, 3));
                this.enemyFleet[2].addPosition(new position(letters.G, 3));
                this.enemyFleet[2].addPosition(new position(letters.H, 3));
        
                this.enemyFleet[3].addPosition(new position(letters.B, 8));
                this.enemyFleet[3].addPosition(new position(letters.B, 7));
                this.enemyFleet[3].addPosition(new position(letters.B, 6));
        
                this.enemyFleet[4].addPosition(new position(letters.A, 1));
                this.enemyFleet[4].addPosition(new position(letters.A, 2));
                break;
            
            case 3:
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleet[0].addPosition(new position(letters.B, 7));
                this.enemyFleet[0].addPosition(new position(letters.B, 8));
        
                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleet[1].addPosition(new position(letters.E, 7));
                this.enemyFleet[1].addPosition(new position(letters.E, 8));
                this.enemyFleet[1].addPosition(new position(letters.E, 9));
        
                this.enemyFleet[2].addPosition(new position(letters.A, 3));
                this.enemyFleet[2].addPosition(new position(letters.B, 3));
                this.enemyFleet[2].addPosition(new position(letters.C, 3));
        
                this.enemyFleet[3].addPosition(new position(letters.F, 8));
                this.enemyFleet[3].addPosition(new position(letters.G, 8));
                this.enemyFleet[3].addPosition(new position(letters.H, 8));
        
                this.enemyFleet[4].addPosition(new position(letters.C, 5));
                this.enemyFleet[4].addPosition(new position(letters.C, 6));
        }
    }

    fillWhitelistList() {
        for (let i = 1; i <= maxRows; i++) {
            for (let j = 1; j <= maxColumns; j++) {
                var currPoss = i + "" + j;
                whiteListedPositionsForAttacking.push(currPoss);
            }
        }
    }

    static dracarys(){
        console.log(cliColor.redBright("(                                             ____ "));
        console.log(cliColor.redBright(")\ )                                         |   / "));
        console.log(cliColor.redBright("(()/(   (       )          )  (    (          |  / "));
        console.log(cliColor.redBright(" /(_))  )(   ( /(   (   ( /(  )(   )\ )  (    | /  "));
        console.log(cliColor.redBright("(_))_  (()\  )(_))  )\  )(_))(()\ (()/(  )\   |/   "));
        console.log(cliColor.redBright("|   \  ((_)((_)_  ((_)((_)_  ((_) )(_))((_) (      "));
        console.log(cliColor.redBright(" | |) || '_|/ _` |/ _| / _` || '_|| || |(_-< )\    "));
        console.log(cliColor.redBright(" |___/ |_|  \__,_|\__| \__,_||_|   \_, |/__/((_)   "));
        console.log(cliColor.redBright("                                   |__/            "));
    }
}

module.exports = Battleship;