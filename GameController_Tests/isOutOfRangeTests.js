const assert = require('assert').strict;
const gameController = require("../GameController/gameController.js");
const letters = require("../GameController/letters.js");
const position = require("../GameController/position.js")

describe('checkIsOutOfRangeTests', function () {
  it('should return false for valid position', function () {
    const actual = gameController.isOutOfRange(new position(letters.A, 1))
    assert.ok(!actual)
  });

  it('should return true for invalid column position', function () {
    const actual = gameController.isOutOfRange(new position(letters.P, 1))
    assert.ok(actual)
  });

  it('should return true for invalid row position', function () {
    const actual = gameController.isOutOfRange(new position(letters.A, 9))
    assert.ok(actual)
  });
});